import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.RemoteException;


public class ClientForm {
    public JPanel panel1;
    private JTextField year;
    private JTextField engine;
    private JTextField price;
    private JButton computeButton;
    private JTextArea textArea1;
    public CommonInterface commonInterface;

    public ClientForm(CommonInterface commonInterface){
        this.commonInterface = commonInterface;
        computeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Car car = new Car(Integer.parseInt(year.getText()), Integer.parseInt(engine.getText()), Integer.parseInt(price.getText()));
                try {
                    double sellingPrice = commonInterface.computePurchasingPrice(car);
                    double tax = commonInterface.computeTax(car);

                    System.out.println("Taxa: " + tax);
                    System.out.println("Selling price: " + sellingPrice);
                    textArea1.insert("TAXA: "+ tax +"\n" + " \nSelling price: " + sellingPrice, 0);

                   // textArea1.insert("Selling price: " + sellingPrice , 50);
                } catch (RemoteException e1) {
                    e1.printStackTrace();
                }

            }
        });
    }

}
