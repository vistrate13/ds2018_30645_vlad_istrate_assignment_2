import javax.swing.*;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class ComputingClient {

    public static void main(String[] args) throws RemoteException, NotBoundException {
        try{
            Registry registry = LocateRegistry.getRegistry(6666);
            CommonInterface commonInterface = (CommonInterface) registry.lookup("Computing");
            if(commonInterface!=null){
                JFrame jFrame = new JFrame("ClientFrom");
                jFrame.setContentPane(new ClientForm(commonInterface).panel1);
                jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                jFrame.setSize(50,50);
                jFrame.pack();
                jFrame.setVisible(true);
            }
        } catch (Exception e){
            e.printStackTrace();
        }
    }
}
