import java.rmi.Remote;
import java.rmi.RemoteException;

public interface CommonInterface extends Remote {

    double computeTax(Car c) throws RemoteException;
    double computePurchasingPrice(Car c) throws RemoteException;
}
