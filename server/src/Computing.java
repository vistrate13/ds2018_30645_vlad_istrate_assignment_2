import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class Computing implements CommonInterface {

    public static void main(String[] args) throws RemoteException {
        try{
            CommonInterface engine = new Computing();
            CommonInterface stub = (CommonInterface) UnicastRemoteObject.exportObject(engine,0);
            Registry registry = LocateRegistry.createRegistry(6666);
            registry.rebind("Computing",stub);
            System.out.println("Bound successful");
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public double computePurchasingPrice(Car c) {
        double priceSelling = 0;
        if(2018 - c.getYear() < 7){
            double div = c.getPrice() / 7;
            priceSelling = c.getPrice() - div*(2018 - c.getYear());
        } else {
            priceSelling = 0;
        }
        return priceSelling;
    }

    @Override
    public double computeTax(Car c) {
        if (c.getEngineSize() <= 0) {
            throw new IllegalArgumentException("Engine capacity must be positive.");
        }
        int sum = 8;
        if(c.getEngineSize() > 1601) sum = 18;
        if(c.getEngineSize() > 2001) sum = 72;
        if(c.getEngineSize() > 2601) sum = 144;
        if(c.getEngineSize() > 3001) sum = 290;
        return c.getEngineSize() / 200.0 * sum;
    }
}
